# README #

This application should (in theory) work as a very basic replacement for ConnectifyMe.
Written in C#, .NET version 3.5, Visual Studio 2010.
Uses a simple "netsh" command that can be accessed manually from command prompt (requires ADMIN permissions).

Clone the repository, use the .exe file located in "WiFi_app / WiFi_app / bin / Debug /" folder.

Warning! 
Caused a BSOD on one of the machines I tested it on, booted just fine afterwards. Tested on only 5 computers. I am not responsible for PC breakdown, house fire or nuclear war. :D

Copyright: none, do whatever you want