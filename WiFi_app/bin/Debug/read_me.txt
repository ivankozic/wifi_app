WiFi_Creator

/*
 * Your warranty is now void.
 *
 * I am not responsible for burning computers, dead hard drives, 
 * thermonuclear war, or you getting fired because the alarm app and/or calendar failed.
 * YOU are choosing to use this free app, and if
 * you point the finger at me for messing up your device, I will laugh at you.  :)
 */
This should pretty much work as a free alternative to apps like Connectify. Hopefully.
Yes, it requires ADMINISTRATOR privileges. No, it does not contain any VIRUSES, bacteria, prions, fungi or any other kind of malicious thingies.

1. start the app
2. press the "Check" button to see if your PC even supports hostednetworks, if yes, proceed
3. in SSID field type in how you want your WiFi hotspot to be named
4. type in password, must be at least 8 characters long and contain at least one number
5. repeat password
6. press "Create hotspot" button

Later you can use buttons "Enable" and "Disable" to enable/disable the hotspot.
You can also do it by hand by going to "Network and Sharing Center" -> "Change adapter settings" -> right-click on connection that
contains "Microsoft Hosted Network Virtual Adapter" and a name that matches with what you enter when you created that hotspot -> press Disable or Enable.

I MAKE NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS
SOFTWARE.  IN NO EVENT SHALL I BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL
DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR
USE OF THIS SOFTWARE.

 - Ivan Kozic, 2015.