﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace WiFi_app
{
    public partial class wifiMainForm : Form
    {
        public wifiMainForm()
        {
            InitializeComponent();
        }

        private void createHotspot_Click(object sender, EventArgs e)
        {
            if (checkHotspotName() == true && checkHotspotPasword() == true)
            {
                netshTerminalCommand("wlan set hostednetwork mode=allow ssid=" + this.ssidBox.Text + " key=" + this.passwordBox.Text);                
                MessageBox.Show("WiFi hotspot named \"" + this.ssidBox.Text + "\" has been created!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
            else
            {
                MessageBox.Show("Failure!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void enableHotspotButton_Click(object sender, EventArgs e)
        {
            netshTerminalCommand("wlan start hostednetwork");
            MessageBox.Show("WiFi hotspot has been enabled!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.None);
        }
        private void disableHotspotButton_Click(object sender, EventArgs e)
        {
            netshTerminalCommand("wlan stop hostednetwork");
            MessageBox.Show("WiFi hotspot has been disabled!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.None);
        }
        /*provjerava je li uopce moguce hostati WiFi na tom uredjaju*/
        private void checkIfCanHost_Click(object sender, EventArgs e)
        {
            bool check=netshTerminalCommand("wlan show drivers");
            if (check == true)
            {
                MessageBox.Show("This devices supports hosted networks!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
           else
                MessageBox.Show("This device does not seem to support hosted networks OR the wireless network device is disable. Enable it and try again.", "Failure!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }



        /* provjerava je li uneseno ime hotspota, da textBox nije prazan */
        /* vraca TRUE ako polje za ime hotspota nije prazno*/
        private bool checkHotspotName()
        {
            bool isNullOrWhiteSpace = StringExtensions.IsNullOrWhiteSpace(this.ssidBox.Text);
            if (isNullOrWhiteSpace == true)
            {
                MessageBox.Show("Hotspot name field is empty!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (this.ssidBox.Text.Contains(" "))
            {
                MessageBox.Show("Hotspot name contains a whitespace!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        /* provjerava je li unesen password, WPA2 format *
        /* vraca TRUE ako je password ispravno unesen, FALSE ako nije */
        private bool checkHotspotPasword()
        {
            /*provjera da password box nije prazan*/
            bool isNullOrWhiteSpace = StringExtensions.IsNullOrWhiteSpace(this.passwordBox.Text);
            if (isNullOrWhiteSpace == true)
            {
                MessageBox.Show("Password field is empty. You must set a password!" , "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            /*provjera da je unesen barem jedan broj za password*/
            else if (!this.passwordBox.Text.Any(Char.IsDigit))
            {
                MessageBox.Show("Password must contain at least one number!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            /*provjerava jesu li lozinka i ponovljena lozinka identicne*/
            else if(!object.Equals(this.passwordBox.Text, this.passwordBoxRepeat.Text))
            {
                MessageBox.Show("Passwords do not match!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            /*provjerava da li lozinka sadrzi " " whitespace*/
            else if (this.passwordBox.Text.Contains(" "))
            {
                MessageBox.Show("Password contains a whitespace!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (this.passwordBox.Text.Length < 8)
            {
                MessageBox.Show("Password needs to be at least 8 characters long!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else
                return true;
        }


        /* posto ovaj "IsNullOrWhiteSpace" nije u NET 3.5 verziji nego tek u NET 4.0, napravio sam svoj */
        /* vraca TRUE ako je string prazan*/
        public static class StringExtensions
        {
            public static bool IsNullOrWhiteSpace(string value)
            {
                if (value != null)
                {
                    for (int i = 0; i < value.Length; i++)
                    {
                        if (!char.IsWhiteSpace(value[i]))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
        }


        
        private bool netshTerminalCommand(string argumentString)
        {
            ProcessStartInfo procInfo = new ProcessStartInfo
            {
                WorkingDirectory = Path.GetPathRoot(Environment.SystemDirectory),
                FileName = Path.Combine(Environment.SystemDirectory, "netsh.exe"),
                Arguments = argumentString,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden
            };

            Process proc = Process.Start(procInfo);
            proc.WaitForExit();

            var systemPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            var completePath = Path.Combine(systemPath, "WiFiCreatorLog.txt");

            while (!proc.StandardOutput.EndOfStream)
            {
                string line = proc.StandardOutput.ReadLine();
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(completePath, true))//na Windows 8.1 file se sprema u C:\ProgramData\WiFiCreatorLog.txt
                {
                    file.WriteLine(DateTime.Now + ": " + line);
                    if (object.Equals("    Hosted network supported  : Yes", line))//MORA imati 4 razmaka prije "Hosted" jer se tako ispisuje u CMDu
                    {
                        return true;
                    }
                } 
            }
            return false;
        }

       
    }
}
