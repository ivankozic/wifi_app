﻿namespace WiFi_app
{
    partial class wifiMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(wifiMainForm));
            this.ssidBox = new System.Windows.Forms.TextBox();
            this.passwordBox = new System.Windows.Forms.TextBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.keyLabel = new System.Windows.Forms.Label();
            this.createHotspotButton = new System.Windows.Forms.Button();
            this.disableHotspotButton = new System.Windows.Forms.Button();
            this.enableHotspotButton = new System.Windows.Forms.Button();
            this.passwordBoxRepeat = new System.Windows.Forms.TextBox();
            this.keyLabelRepeat = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.checkIfCanHost = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ssidBox
            // 
            this.ssidBox.Location = new System.Drawing.Point(125, 12);
            this.ssidBox.Name = "ssidBox";
            this.ssidBox.Size = new System.Drawing.Size(150, 20);
            this.ssidBox.TabIndex = 0;
            // 
            // passwordBox
            // 
            this.passwordBox.Location = new System.Drawing.Point(125, 38);
            this.passwordBox.Name = "passwordBox";
            this.passwordBox.Size = new System.Drawing.Size(150, 20);
            this.passwordBox.TabIndex = 1;
            this.passwordBox.UseSystemPasswordChar = true;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(12, 12);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(107, 13);
            this.nameLabel.TabIndex = 6;
            this.nameLabel.Text = "Hotspot name (SSID)";
            // 
            // keyLabel
            // 
            this.keyLabel.AutoSize = true;
            this.keyLabel.Location = new System.Drawing.Point(12, 38);
            this.keyLabel.Name = "keyLabel";
            this.keyLabel.Size = new System.Drawing.Size(107, 13);
            this.keyLabel.TabIndex = 7;
            this.keyLabel.Text = "Network key (WPA2)";
            // 
            // createHotspotButton
            // 
            this.createHotspotButton.Location = new System.Drawing.Point(190, 104);
            this.createHotspotButton.Name = "createHotspotButton";
            this.createHotspotButton.Size = new System.Drawing.Size(85, 23);
            this.createHotspotButton.TabIndex = 4;
            this.createHotspotButton.Text = "Create hotspot";
            this.createHotspotButton.UseVisualStyleBackColor = true;
            this.createHotspotButton.Click += new System.EventHandler(this.createHotspot_Click);
            // 
            // disableHotspotButton
            // 
            this.disableHotspotButton.Location = new System.Drawing.Point(190, 133);
            this.disableHotspotButton.Name = "disableHotspotButton";
            this.disableHotspotButton.Size = new System.Drawing.Size(85, 23);
            this.disableHotspotButton.TabIndex = 6;
            this.disableHotspotButton.Text = "Disable";
            this.disableHotspotButton.UseVisualStyleBackColor = true;
            this.disableHotspotButton.Click += new System.EventHandler(this.disableHotspotButton_Click);
            // 
            // enableHotspotButton
            // 
            this.enableHotspotButton.Location = new System.Drawing.Point(99, 133);
            this.enableHotspotButton.Name = "enableHotspotButton";
            this.enableHotspotButton.Size = new System.Drawing.Size(85, 23);
            this.enableHotspotButton.TabIndex = 5;
            this.enableHotspotButton.Text = "Enable";
            this.enableHotspotButton.UseVisualStyleBackColor = true;
            this.enableHotspotButton.Click += new System.EventHandler(this.enableHotspotButton_Click);
            // 
            // passwordBoxRepeat
            // 
            this.passwordBoxRepeat.Location = new System.Drawing.Point(125, 64);
            this.passwordBoxRepeat.Name = "passwordBoxRepeat";
            this.passwordBoxRepeat.Size = new System.Drawing.Size(150, 20);
            this.passwordBoxRepeat.TabIndex = 2;
            this.passwordBoxRepeat.UseSystemPasswordChar = true;
            // 
            // keyLabelRepeat
            // 
            this.keyLabelRepeat.AutoSize = true;
            this.keyLabelRepeat.Location = new System.Drawing.Point(16, 64);
            this.keyLabelRepeat.Name = "keyLabelRepeat";
            this.keyLabelRepeat.Size = new System.Drawing.Size(103, 13);
            this.keyLabelRepeat.TabIndex = 8;
            this.keyLabelRepeat.Text = "Repeat network key";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label1.Location = new System.Drawing.Point(12, 170);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Ivan Kozic 2015 ©";
            // 
            // checkIfCanHost
            // 
            this.checkIfCanHost.Location = new System.Drawing.Point(99, 104);
            this.checkIfCanHost.Name = "checkIfCanHost";
            this.checkIfCanHost.Size = new System.Drawing.Size(85, 23);
            this.checkIfCanHost.TabIndex = 3;
            this.checkIfCanHost.Text = "Check";
            this.checkIfCanHost.UseVisualStyleBackColor = true;
            this.checkIfCanHost.Click += new System.EventHandler(this.checkIfCanHost_Click);
            // 
            // wifiMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 193);
            this.Controls.Add(this.checkIfCanHost);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.keyLabelRepeat);
            this.Controls.Add(this.passwordBoxRepeat);
            this.Controls.Add(this.enableHotspotButton);
            this.Controls.Add(this.disableHotspotButton);
            this.Controls.Add(this.createHotspotButton);
            this.Controls.Add(this.keyLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.passwordBox);
            this.Controls.Add(this.ssidBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "wifiMainForm";
            this.Text = "Create WiFi hotspot";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ssidBox;
        private System.Windows.Forms.TextBox passwordBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label keyLabel;
        private System.Windows.Forms.Button createHotspotButton;
        private System.Windows.Forms.Button disableHotspotButton;
        private System.Windows.Forms.Button enableHotspotButton;
        private System.Windows.Forms.TextBox passwordBoxRepeat;
        private System.Windows.Forms.Label keyLabelRepeat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button checkIfCanHost;
    }
}

